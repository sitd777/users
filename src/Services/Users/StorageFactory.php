<?php

namespace Services\Users;

use Core\App;
use Core\Cache;

/**
 * Class StorageFactory
 * @package Services\Users
 */
class StorageFactory
{
    /**
     * Gets storage type
     * @return string
     */
    public static function getStorage()
    {
        return Cache::get('__storage') ?? 'file';
    }

    /**
     * Set storage type
     * @param $value
     */
    public static function setStorage($value)
    {
        $storageClass = '\\Services\\Users\\Storage' . App::convertToStudlyCaps($value);
        if(class_exists($storageClass)) {
            Cache::set('__storage', $value);
        }
    }

    /**
     * Storage factory
     * @return mixed
     * @throws \Exception
     */
    public static function factory()
    {
        $storage = static::getStorage();
        $storageClass = '\\Services\\Users\\Storage' . App::convertToStudlyCaps($storage);
        if(!class_exists($storageClass)) {
            throw new \Exception('Storage is not found: ' . $storage);
        }

        return new $storageClass();

    }
}