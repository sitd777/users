<?php

namespace Services\Users;

use Core\App;
use Core\Logger;

/**
 * Class StorageFile
 * @package Services\Users
 */
class StorageFile implements StorageInterface
{
    /**
     * Loads data from file
     * @return mixed
     */
    public function loadData()
    {
        $file = $this->getStorageFile();

        if(file_exists($file)) {
            $users = unserialize(file_get_contents($this->getStorageFile()));
        } else {
            $users = [];
        }

        Logger::log('Load from file');

        return $users;
    }

    /**
     * Saves new data into the file
     * @param mixed $data
     * @return mixed
     */
    public function saveData($data)
    {
        file_put_contents($this->getStorageFile(), serialize($data));

        Logger::log('Fill file');
    }

    /**
     * Removes user with UUID
     * @param $uuid
     */
    public function removeItem($uuid)
    {
        $users = $this->loadData();
        unset($users[$uuid]);
        $this->saveData($users);
    }

    /**
     * Returns file path for data
     * @return array|mixed|null
     */
    protected function getStorageFile()
    {
        return App::baseDir() . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'runtime' . DIRECTORY_SEPARATOR . 'users';
    }
}