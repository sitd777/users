<?php

namespace Services\Users;

use Core\App;
use Core\Logger;

/**
 * Class SourceRandomuser
 * @package Services\Users
 */
class SourceRandomuser implements SourceInterface
{
    /**
     * Loads data from randomuser.me
     * @return mixed
     */
    public function getData()
    {
        $users = [];
        foreach (json_decode(file_get_contents('https://randomuser.me/api/?results=5&nat=gb'), true)['results'] as $data) {
            $uuid = uniqid();
            $users[$uuid] = [
                'uuid' => $uuid,
                'first_name' =>  $data['name']['first'],
                'last_name' =>  $data['name']['last'],
                'location' => $data['location'],
                'email' => $data['email'],
                'phone' => $data['phone'],
                'registered_at' => date_create($data['registered']['date']),
            ];
        }
        return $users;
    }

    /**
     * Returns file path for data
     * @return array|mixed|null
     */
    protected function getUrl()
    {
        return 'https://randomuser.me/api/?results=5&nat=gb';
    }
}