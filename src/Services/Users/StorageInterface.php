<?php

namespace Services\Users;

/**
 * Interface StorageInterface
 * @package Services\Users
 */
interface StorageInterface
{
    public function loadData();

    public function saveData($data);
}