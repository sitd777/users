<?php

namespace Services\Users;

/**
 * Interface SourceInterface
 * @package Services\Users
 */
interface SourceInterface
{
    public function getData();
}