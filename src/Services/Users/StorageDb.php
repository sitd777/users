<?php

namespace Services\Users;

use \PDO;
use Core\App;
use Core\Logger;
use Core\Cache;

/**
 * Class StorageFile
 * @package Services\Users
 */
class StorageDb implements StorageInterface
{
    /**
     * Holds PDO object
     * @var null
     */
    protected $pdo = null;

    public function __construct()
    {
        $this->pdo = new PDO('sqlite:' . $this->getStorageFile(), 'root', '', ['driver' => 'pdo_sqlite']);

        $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $this->pdo->exec('
        CREATE TABLE IF NOT EXISTS users (
          uuid VARCHAR(255) NOT NULL,
          first_name VARCHAR(255) NOT NULL,
          last_name VARCHAR(255) NOT NULL,
          phone VARCHAR(255) DEFAULT NULL,
          email VARCHAR(255) DEFAULT NULL,
          address TEXT NOT NULL,
          registered_at DATETIME NOT NULL,
          PRIMARY KEY (uuid)
        )
        ');
    }

    /**
     * Loads data from db
     * @return mixed
     */
    public function loadData()
    {
        $users = Cache::get('db');

        if ($users === null) {
            foreach ($this->pdo->query('SELECT * FROM users')->fetchAll() as $row) {
                $users[$row['uuid']] = [
                    'uuid' => $row['uuid'],
                    'first_name' =>  $row['first_name'],
                    'last_name' =>  $row['last_name'],
                    'location' => json_decode($row['address'], true),
                    'email' => $row['email'],
                    'phone' => $row['phone'],
                    'registered_at' => date_create($row['registered_at']),
                ];
            }
            if (! $users) {
                $users = [];
            }
            Cache::set('db', $users);
            Logger::log('Load from db');
        } else {
            Logger::log('Load from cache');
        }

        return $users;
    }

    /**
     * Saves new data into the db
     * @param mixed $data
     * @return mixed
     */
    public function saveData($data)
    {
        $this->pdo->exec('DELETE FROM users');

        foreach ($data as $uuid => $user) {
            $serializedLocation = json_encode($user['location']);
            $this->pdo->exec("INSERT INTO users (uuid, first_name, last_name, email, phone, address, registered_at) VALUES 
          (
            '$uuid', 
            '{$user['first_name']}', 
            '{$user['last_name']}', 
            '{$user['email']}', 
            '{$user['phone']}', 
            '{$serializedLocation}', 
            '{$user['registered_at']->format('Y-m-d H:i:s')}'
        )");
        }

        Cache::reset('db');
        Logger::log('Fill db');
    }

    /**
     * Removes user with UUID
     * @param $uuid
     */
    public function removeItem($uuid)
    {
        $this->pdo->exec('DELETE FROM users WHERE uuid = "' . $uuid . '"');

        Cache::reset('db');

        Logger::log('Remove from db ' . $uuid);
    }

    /**
     * Returns file path for data
     * @return array|mixed|null
     */
    protected function getStorageFile()
    {
        return App::baseDir() . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'runtime' . DIRECTORY_SEPARATOR . 'db.sqlite3';
    }
}