<?php

namespace Services\Users;

use Core\App;

/**
 * Class SourceFactory
 * @package Services\Users
 */
class SourceFactory
{
    /**
     * Holds source type
     * @var string
     */
    protected static $source = 'randomuser';

    /**
     * Storage factory
     * @param string $source
     * @return mixed
     * @throws \Exception
     */
    public static function factory()
    {
        $sourceClass = '\\Services\\Users\\Source' . App::convertToStudlyCaps(static::$source);
        if(!class_exists($sourceClass)) {
            throw new \Exception('Source is not found: ' . static::$source);
        }

        return new $sourceClass();
    }
}