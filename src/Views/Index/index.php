<?php
/**
 * @var array $users
 */
?>

<table class="table">
    <thead>
    <tr>
        <th scope="col">#</th>
        <th scope="col">Имя</th>
        <th scope="col">Фамилия</th>
        <th scope="col">Телефон</th>
        <th scope="col">Email</th>
        <th scope="col">Адрес</th>
        <th scope="col">Зарегистрирован</th>
        <th scope="col"></th>
    </tr>
    </thead>
    <tbody>
    <?php
    $number = 1;
    foreach ($users as $user) {
        $date = $user['registered_at'];
        $monthes = [
            '01' => 'января',
            '02' => 'февраля',
            '03' => 'марта',
            '04' => 'апреля',
            '05' => 'мая',
            '06' => 'июня',
            '07' => 'июля',
            '08' => 'августа',
            '09' => 'сентября',
            '10' => 'октября',
            '11' => 'ноября',
            '12' => 'декабря',
        ];
        $month = $monthes[$user['registered_at']->format('m')];

        ?>
        <tr>
            <th scope="row"><?= $number++ ?></th>
            <td><?= $user['first_name']?></td>
            <td><?= $user['last_name']?></td>
            <td><?= $user['phone']?></td>
            <td><?= $user['email']?></td>
            <td><?= implode(', ', $user['location']) ?></td>
            <td><?= $date->format('d ' . $month . ' Y H:i:s') ?></td>
            <td>
                <a href="<?= \Core\App::url('remove?uuid=' . $user['uuid'] )?>" class="btn btn-danger">Удалить</a>
            </td>
        </tr>
        <?php
    }
    ?>
    </tbody>
</table>
<div class="container-fluid">
    <a class="btn btn-success float-right" href="<?= \Core\App::url('generate')?>" role="button">Сгенерировать</a>
</div>
