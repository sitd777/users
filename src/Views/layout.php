<?php

use Core\App;
use Services\Users\StorageFactory;

$action = 'load_from_file';
?>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Test Project</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="<?= App::baseUrl() ?>css/style.css">
</head>
<body>

<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <ul class="nav nav-pills">
        <li class="nav-item">
            <a class="nav-link <?= (StorageFactory::getStorage() == 'file' ? 'active' : '') ?>" href="<?= App::url('change?source=file') ?>">Файл</a>
        </li>
        <li class="nav-item">
            <a class="nav-link <?= (StorageFactory::getStorage() == 'db' ? 'active' : '') ?>" href="<?= App::url('change?source=db') ?>">База данных</a>
        </li>
    </ul>
</nav>

<div class="container">
    <?= $content ?>
</div>

<script src="<?= App::baseUrl() ?>js/script.js"></script>
</body>
</html>


