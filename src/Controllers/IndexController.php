<?php

namespace controllers;

use Core\App;
use Services\Users\StorageFactory;
use Services\Users\SourceFactory;

/**
 * Class IndexController
 * @package controllers
 */
class IndexController extends BaseController
{
    /**
     * Shows list of users
     * @throws \Exception
     */
    public function actionIndex()
    {
        $storage = StorageFactory::factory();
        return $this->render('index', ['users' => $storage->loadData()]);
    }

    /**
     * Generates new data
     * @throws \Exception
     */
    public function actionGenerate()
    {
        $storage = StorageFactory::factory();
        $source  = SourceFactory::factory();
        $storage->saveData( $source->getData() );

        return $this->redirect(App::url('index'));
    }

    /**
     * Removes user from list
     * @throws \Exception
     */
    public function actionRemove()
    {
        $uuid = $_GET['uuid'] ?? 0;

        if($uuid) {
            $storage = StorageFactory::factory();
            $storage->removeItem($uuid);
        }

        return $this->redirect(App::url('index'));
    }

    /**
     * Changes source for users data
     */
    public function actionChange()
    {
        $source = $_GET['source'] ?? '';

        if($source) {
            StorageFactory::setStorage($source);
        }

        return $this->redirect(App::url('index'));
    }
}