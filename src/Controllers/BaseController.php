<?php

namespace controllers;

use Core\App;

/**
 * Class BaseController
 * @package controllers
 */
abstract class BaseController
{
    /**
     * Current layout
     * @var string
     */
    public $layout = 'layout';

    /**
     * Renders full page
     * @param string $view
     * @param array $params
     */
    protected function render($view, $params = [])
    {
        $content = $this->renderPartial($view, $params);

        $viewBase = App::baseDir() . '\\Views\\';
        $layoutFile = App::convertToCamelCase($this->layout) . '.php';
        if(file_exists($viewBase . $layoutFile)) {
            require_once $viewBase . $layoutFile;
            die;
        }

        throw new \Exception("Layout file `$layoutFile` not found");
    }

    /**
     * Renders content for a page
     * @param string $view
     * @param array $params
     * @return string
     * @throws \Exception
     */
    protected function renderPartial($view, $params = [])
    {
        $tmp = explode('\\', get_class($this));
        $controllerName = substr(array_pop($tmp), 0, -10);
        $viewBase = App::baseDir() . '\\Views\\';
        $viewFile = $controllerName . '\\' . App::convertToCamelCase($view) . '.php';

        if(file_exists($viewBase . $viewFile)) {
            extract($params, EXTR_OVERWRITE);
            ob_start();
            require $viewBase . $viewFile;
            return ob_get_clean();
        }

        throw new \Exception("View file `$viewFile` not found");
    }

    /**
     * Redirects page
     * @param $url
     */
    protected function redirect($url)
    {
        header('Location: ' . $url);
        die;
    }
}