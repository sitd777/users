<?php

namespace Core;

use \Core\App;

class Cache
{
    /**
     * Returns filename for cached value
     * @param $key
     * @return string
     */
    protected static function getCachePath($key)
    {
        return App::baseDir() . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'runtime' .
            DIRECTORY_SEPARATOR . 'cache' . DIRECTORY_SEPARATOR . $key;
    }

    /**
     * Sets cache value
     * @param $key
     * @param $value
     */
    public static function set($key, $value)
    {
        $path = static::getCachePath($key);

        if (! is_dir(dirname($path))) {
            mkdir(dirname($path), 0777, true);
        }

        file_put_contents($path, sprintf("<?php\nreturn %s;", var_export($value, true)));
    }

    /**
     * Returns cache value
     * @param $key
     * @return mixed|null
     */
    public static function get($key)
    {
        $path = static::getCachePath($key);

        if (is_file($path)) {
            return include $path;
        }

        return null;
    }

    /**
     * Checks existing of value
     * @param $key
     * @return bool
     */
    public static function has($key)
    {
        $path = static::getCachePath($key);

        return is_file($path);
    }

    /**
     * Resets cached value
     * @param $key
     */
    public static function reset($key)
    {
        $path = static::getCachePath($key);

        if (is_file($path)) {
            unlink($path);
        }
    }
}