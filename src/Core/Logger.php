<?php

namespace Core;

use \Core\App;

class Logger
{
    public static function log($message)
    {
        $h = fopen(App::baseDir() . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'runtime' . DIRECTORY_SEPARATOR . 'log', 'a');
        fwrite($h, date('d.m.Y H:i:s') . ' - ' . $message . "\n\n");
        fclose($h);
    }
}