<?php

namespace Core;

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

/**
 * Class App
 * @package Core
 */
class App
{
    /**
     * Application configuration
     * @var array
     */
    protected static $config = [];

    /**
     * Holds base doirectory
     * @var string
     */
    protected static $baseDir = '';

    /**
     * Holds base URL
     * @var string
     */
    protected static $baseUrl = '';

    /**
     * Holds database entity manager
     * @var null
     */
    public static $entityManager = null;

    /**
     * Default controller
     * @var string
     */
    public static $defaultController = 'index';

    /**
     * Default action
     * @var string
     */
    public static $defaultAction = 'index';

    /**
     * Current controller
     * @var string
     */
    public static $controller = '';

    /**
     * Current action
     * @var string
     */
    public static $action = '';

    /**
     * Initialize system
     */
    public static function init($config = [])
    {
        static::$baseDir = dirname(__DIR__);

        $path = explode('/', $_SERVER['SCRIPT_NAME']);
        array_pop($path);
        static::$baseUrl = implode('/', $path) . '/';

        static::$config = $config;
        static::db();
        static::route();
    }

    /**
     * Returns configuration info
     * @var string $param
     */
    public static function getConfig($param = null)
    {
        if($param) {
            return static::$config[$param] ?? null;
        }
        return static::$config;
    }

    /**
     * Returns base directory
     * @return string
     */
    public static function baseDir()
    {
        return static::$baseDir;
    }

    /**
     * Returns base URL
     * @return string
     */
    public static function baseUrl()
    {
        return static::$baseUrl;
    }

    /**
     * Generates an URL
     * @param $url
     * @return string
     */
    public static function url($url)
    {
        return static::baseUrl() . $url;
    }

    /**
     * Configure database connection
     */
    public static function db()
    {
    }

    /**
     * Find controller and execute action
     * @throws \Exception
     */
    public static function route()
    {
        $url = @$_REQUEST['url'];
        $parts = explode('/', trim($url, '/'), 2);

        if(count($parts) >= 2) {
            static::$controller = static::convertToStudlyCaps(!empty($parts[0]) ? $parts[0] : static::$defaultController);
            static::$action = static::convertToStudlyCaps(!empty($parts[1]) ? $parts[1] : static::$defaultAction);
        } else if(count($parts) == 1) {
            static::$controller = static::convertToStudlyCaps(static::$defaultController);
            static::$action = static::convertToStudlyCaps(!empty($parts[0]) ? $parts[0] : static::$defaultAction);
        } else {
            static::$controller = static::convertToStudlyCaps(static::$defaultController);
            static::$action = static::convertToStudlyCaps(static::$defaultAction);
        }

        $controllerName = '\\Controllers\\' . static::$controller . 'Controller';
        if (class_exists($controllerName)) {
            $actionName = 'action' . static::$action;
            $controllerObject = new $controllerName();
            if (method_exists($controllerObject, $actionName)) {
                $controllerObject->$actionName();
            } else {
                throw new \Exception("Method $actionName in controller $controllerName not found");
            }
        } else {
            throw new \Exception("Controller class $controllerName not found");
        }
    }

    /**
     * Convert the string with hyphens to StudlyCaps,
     * e.g. post-authors => PostAuthors
     *
     * @param string $string The string to convert
     * @return string
     */
    public static function convertToStudlyCaps($string)
    {
        return str_replace(' ', '', ucwords(str_replace('-', ' ', $string)));
    }

    /**
     * Convert the string with hyphens to camelCase,
     * e.g. add-new => addNew
     *
     * @param string $string The string to convert
     * @return string
     */
    public static function convertToCamelCase($string)
    {
        return lcfirst(static::convertToStudlyCaps($string));
    }

    /**
     * Remove the query string variables from the URL (if any). As the full
     * query string is used for the route, any variables at the end will need
     * to be removed before the route is matched to the routing table. For
     * example:
     *
     *   URL                           $_SERVER['QUERY_STRING']  Route
     *   -------------------------------------------------------------------
     *   localhost                     ''                        ''
     *   localhost/?                   ''                        ''
     *   localhost/?page=1             page=1                    ''
     *   localhost/posts?page=1        posts&page=1              posts
     *   localhost/posts/index         posts/index               posts/index
     *   localhost/posts/index?page=1  posts/index&page=1        posts/index
     *
     * A URL of the format localhost/?page (one variable name, no value) won't
     * work however. (NB. The .htaccess file converts the first ? to a & when
     * it's passed through to the $_SERVER variable).
     *
     * @param string $url The full URL
     *
     * @return string The URL with the query string variables removed
     */
    protected function removeQueryStringVariables($url)
    {
        if ($url != '') {
            $parts = explode('&', $url, 2);

            if (strpos($parts[0], '=') === false) {
                $url = $parts[0];
            } else {
                $url = '';
            }
        }

        return $url;
    }
}
